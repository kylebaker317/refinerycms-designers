# Designers extension for Refinery CMS.

## How to build this extension as a gem (not required)

    cd vendor/extensions/designers
    gem build refinerycms-designers.gemspec
    gem install refinerycms-designers.gem

    # Sign up for a https://rubygems.org/ account and publish the gem
    gem push refinerycms-designers.gem
