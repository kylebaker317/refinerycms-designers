
FactoryGirl.define do
  factory :designer, :class => Refinery::Designers::Designer do
    sequence(:title) { |n| "refinery#{n}" }
  end
end

