Refinery::Core::Engine.routes.draw do

  # Frontend routes
  # namespace :designers do
  #   resources :designers, :path => '', :only => [:index, :show]
  # end
  scope 'about/our-team', module: 'designers', as: 'designers' do
    resources :designers, :path => '', :only => [:index, :show]
  end

  # Admin routes
  namespace :designers, :path => '' do
    namespace :admin, :path => Refinery::Core.backend_route do
      resources :designers, :except => :show do
        collection do
          post :update_positions
        end
      end
    end
  end

end
