module Refinery
  module Designers
    class Designer < Refinery::Core::BaseModel
      self.table_name = 'refinery_designers'


      validates :name, presence: true, uniqueness: true
      validates :title, presence: true

      belongs_to :photo, class_name: '::Refinery::Image'

      # To enable admin searching, add acts_as_indexed on searchable fields, for example:
      #
      #   acts_as_indexed fields: [:title]

    end
  end
end
