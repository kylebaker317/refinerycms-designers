module Refinery
  module Designers
    class DesignersController < ::ApplicationController

      before_action :find_all_designers
      before_action :find_page

      def index
        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @designer in the line below:
        present(@page)
      end

      def show
        @designer = Designer.find(params[:id])

        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @designer in the line below:
        present(@page)
      end

    protected

      def find_all_designers
        @designers = Designer.order('position ASC')
      end

      def find_page
        @page = ::Refinery::Page.where(:link_url => "/about/our-team").first
      end

    end
  end
end
