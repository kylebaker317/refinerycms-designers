module Refinery
  module Designers
    module Admin
      class DesignersController < ::Refinery::AdminController

        crudify :'refinery/designers/designer',
                title_attribute: 'name'

        private

        # Only allow a trusted parameter "white list" through.
        def designer_params
          params.require(:designer).permit(:title, :name, :photo_id, :description)
        end
      end
    end
  end
end
