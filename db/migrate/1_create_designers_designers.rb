class CreateDesignersDesigners < ActiveRecord::Migration

  def up
    create_table :refinery_designers do |t|
      t.string :title
      t.string :name
      t.integer :photo_id
      t.text :description
      t.integer :position

      t.timestamps
    end

  end

  def down
    if defined?(::Refinery::UserPlugin)
      ::Refinery::UserPlugin.destroy_all({:name => "refinerycms-designers"})
    end

    if defined?(::Refinery::Page)
      ::Refinery::Page.delete_all({:link_url => "/designers/designers"})
    end

    drop_table :refinery_designers

  end

end
