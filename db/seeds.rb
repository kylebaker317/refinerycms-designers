Refinery::I18n.frontend_locales.each do |lang|
  I18n.locale = lang

  Refinery::User.find_each do |user|
    user.plugins.where(name: 'refinerycms-designers').first_or_create!(
        position: (user.plugins.maximum(:position) || -1) +1
    )
  end if defined?(Refinery::User)

  parent_id = Refinery::Page.by_title('About').first.id
  Refinery::Page.where(link_url: (url = "/about/our-team")).first_or_create!(
      title: 'Our Team',
      deletable: false,
      menu_match: "^#{url}(\/|\/.+?|)$",
      parent_id: parent_id
  ) do |page|
    Refinery::Pages.default_parts.each_with_index do |part, index|
      page.parts.build title: part[:title], slug: part[:slug], body: nil, position: index
    end
  end if defined?(Refinery::Page)
end