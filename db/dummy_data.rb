Refinery::I18n.frontend_locales.each do |lang|
  I18n.locale = lang

  # Created Seeds
  def create_dummy_services
    # Refinery::Designers::Designer(id: integer, title: string, name: string, description: text, position: integer,
    # created_at: datetime, updated_at: datetime, photo_id: integer)
    designers = [{name: "Julie Boutilier", title: 'CEO'},
                 {name: "Marissa", title: 'Designer'},
                 {name: "Kailyn Baker", title: 'Designer'}]

    designers.each do |designer|
      Refinery::Designers::Designer.create title: designer[:title], name: designer[:name],
                                           description: Faker::Lorem.paragraph(2, false, 4)
    end
  end

  create_dummy_services
  puts 'Refinery::Designers dummy_data.rb ran successfully'
end
