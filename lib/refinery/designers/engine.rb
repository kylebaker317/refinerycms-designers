module Refinery
  module Designers
    class Engine < Rails::Engine
      extend Refinery::Engine
      isolate_namespace Refinery::Designers

      engine_name :refinery_designers

      initializer "designers.assets.precompile" do |app|
        Rails.application.config.assets.precompile += %w( refinery/services/frontend.css )
      end

      before_inclusion do
        Refinery::Plugin.register do |plugin|
          plugin.name = "designers"
          plugin.url = proc { Refinery::Core::Engine.routes.url_helpers.designers_admin_designers_path }
          plugin.pathname = root

        end
      end


      config.after_initialize do
        Refinery.register_extension(Refinery::Designers)
      end
    end
  end
end
